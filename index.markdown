---
layout: default
---

# pac85 religione

venerare pac85 ha smesso di essere un meme, è ora di fondare una religione basata sul culto di questa persona.

a pac ci piace:

+ il free software
+ archlinux
+ rantare sul free software
+ i treni

Puntiamo ad entrare nelle [top 22 religioni del mondo](https://www.adherents.com/Religions_By_Adherents.html), grande pac aiutaci.

Benvenuto in pac85.club, l'unico fan club ufficiale dell'OG fossaro @pac85!
Ok, prima di tutto, ti dobbiamo un po' di chiarimenti.

## F.A.Q.

**Q: Perché sono qui?**
A: non lo so

**Q: Siete consapevoli di avere hostato il cringe?**
A: sì

**Q: Perché esiste questo sito?**
A: perché pac si merita la gloria tipo mondiale

**Q: Sono Kezi, ti posso bannare dal flames network?**
A: forse

# I fondamenti del Pacanesimo

Bella. Fatti i chiarimenti, passiamo al dunque. pac85.club è una **organizzazione religiosa senza scopo di lucro**. Pensa Scientology, però FOSS e senza software proprietario. Il nostro credo si basa sul Free Open Source Software, nostra linfa vitale e cibo della nostra anima. La nostra religione segue la filosofia KISS: al contrario di altre religioni più conosciute, infatti, il nostro credo è molto chiaro e open source. Al contrario di altre religioni, infatti, non nasconderemo mai odio nei confronti di gruppi di persone dietro ad un testo di culto[1], non abbiamo agende politiche[2] o scopo di lucro.


+ [1] eccetto gli utenti di obontoo

+  [2] chiunque lavora ad nvidia deve essere imprigionato senza eccezioni

## I 1001 comandamenti

* __0000__:	Userai solo software FOSS.
* __0001__:	Non commetterai peccati mortali come l'installazione di Windows, l'installazione di macOS, o di alcun sistema operativo proprietario. Boicotterai, inoltre, WSL. (nikisalli stai attento)
* __0010__:	Non avrai altro fossaro all'infuori di pac85. Il Pacanesimo è una religione monoteista, non vengono accettate altre divinità.
* __0011__:	Confortemente ai Misteri del Pacanesimo, oltre che adorare Pac, dovrai diffidare da lui.
* __0100__:	Non commetterai atti sessualmente impuri: la `figa` è deprecata, tanto il muflone l'ha tolta dalla AUR, ergo, il sesso è deprecato, ergo, che schifo la figa, meglio Linux
* __0101__:	La migliore distro è **Arch Linux**. Non userai derivate di Arch Linux all'infuori di Parabola GNU/Linux-libre, la quale viene da noi considerata una via per la santità. Se non usi Arch Linux, la tua distro deve essere approvata dal Consiglio dei Sacerdoti, altrimenti, rischi di utilizzare inconsapevolmente una distro impura che ti annebbia l'anima.
* __0110__:	Non disonorerai il free software. Frasi come "Viva Windows", "Linux fa cagare" o "Che schifo `vim`, uso vscode" costituiscono un peccato (paccato) mortale e vanno confessate ed espiate piallando l'SSD, cancellando Windows e installando Arch Linux senza script e senza leggere la beginner's guide dal telefono (leggere davvero le manpage è una via per la beatitudine) (arves100 stai attento). Non troverai mai un difetto a Linux: linux è l'essenza della perfezione, da Xorg a Pulseaudio, da pacman a systemd.
* __0111__: Non pubblicherai software closed-source (leenocs e kezi state attenti)
* __1000__:	Non invidierai la licenza altrui. Esiste solo la GPL, massimo AGPL. 
* __1001__: Non avrai altra laurea all'infuori di CS. Gli ingegneri sono accettati dal pacanesimo, ad ogni modo, difficilmente raggiungeranno la via della salvezza. (nik stai attento perché stai veramente camminando su ghiaccio sottilissimo)

[perle di pac](https://rcastellotti.gitlab.io/pac85.club/pacperle)


## I testi sacri

* Arch Linux Wiki [link](https://wiki.archlinux.org)
* K&R - The C Programming Language
* Andrew S. Tanenbaum, Todd Austin - Structured Computer Organization
* The Rust Programming language [link](https://doc.rust-lang.org/book/)


