---
layout: default
---

# kezi, la grande guida
Un giorno, mentre keziolioburromargarina stava configurando la sua install di arch decise di regalare a noi comuni mortali il [flames network](https://t.me/flamesnetwork), un gruppo di gruppi (lol) di nerd sfigati che si riunisce in base a vari interessi ed ha una comune caratteristica, l' organizzazione degli admin che è la seguente:
+ COMANDA
+ SOLO
+ ED 
+ ESCLUSIVAMENTE
+ KEZI
+ (è il default del resto)

## cose che ci piacciono a keziolioburromargarina

+ DEFAULT
+ ELETTRONICA (in realtà sa a malapena accendere un rpi)
+ ARCHLINUX
+ KDE
+ DETENERE IL POTERE ASSOLUTO
+ MENARLA A CHIUNQUE NON USI ARCH
+ SIGTERM (e non ti da il sorgente, così just 4 fun)

## about kezi
Qualcuno dice che kezi in realtà non esiste, è solamente un bot programmato dal grande sigterm (che svolge la funzione di psicopolizia orwelliana) e che si è da solo fatto il sito [sigterm.cc](http://sigterm.cc). 
A kezi piacciono i default, oramai sono diventati per lui un ossessione, c'è chi dice che sia eterosessuale solo perchè è il default, usa temi talmente chiari che i suoi occhi sono andati via qualche tempo fa, anche perchè usa lo sfondo di default di Kde, di un colorino blu raccomandato da 9 oculisti su 10.

## kezi divinità
In un mondo di soli seguaci di pac85 è molto importante avere anche altre divinità da venerare, e keziolioburromargarina costituisce una valida alternativa, il suo lifestyle è stato fondamentale per molti piccoli linuxini che sono influenzati dal grande maestro, il quale gira nudo (è il default), compila pacchetti per archittetture strane e posta video crizzi su youtube con il solo intento di fare vedere quanto è bello lui. E' un grande programmatore, e ama linguaggi quali Python, Javascript e Lua.

## soluzione kezi
Kezi è dotato di default di uno spirito di problem solving molto semplice, la risposta è sempre la stessa, segue qualche esempio:

__Q: come si fixa questo errore di packagemanager != pacman)__ 

A: installa arch e usa pacman

__Q: dove trovo questo pacchetto? COPR? PPA?__

A: lol AUR

__Q: come si fa a fare l' update?__

A: lol installa arch

__Q: mi si è rotto arch, come fixo?__

A: arch non si rompe, sei scarso tu

__Q: mi consigliate una distro?__

A: arch

__Q: ho problemi con la fidanzata, che faccio?__

A: installa arch

__Q: come si configura/applica un tema?__

A:i DeFaUlT SoNo lA cOsA mIgLiOreH

__Q: ho appena installato arch che faccio?__

A: reinstalla arch

__Q: come si sistema X?__

A: nessuno usa X installa Y

## kezi lifestyle
ecco come si svolge la giornata tipo di keziolioburromargarina

12:10: sveglia

12:20: colazione

12:30: colazione gatti

13:00: pacman -Syu

13:30: rm -rf ~/.config

14:00: pranzo

14:30 foto al gatto per ignu

14:45 scrivere a pac

15:00 flame su Linux ita

16:00 flame con pac

17:00 flame su elettronica ita

18:00 keziolata


[perle di kezi](https://rcastellotti.gitlab.io/pac85.club/keziperle)




